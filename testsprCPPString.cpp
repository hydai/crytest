#include <time.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
using namespace std;
extern void md5(const uint8_t *initial_msg, size_t initial_len, uint8_t *digest);
int main(int argc, char *argv[])
{
    clock_t start, end;
    int i;
    char oao[10000];
    string AA = "SpecChangeAgain";
    long long int BB = 1000000000;
    uint8_t result[16];
    printf("Start testing\n");
    printf("C++ mode with C++-style header and string\n");
    start = clock();
    for (i = 0; i < 500000; i++) {
        sprintf(oao, "%s%lld", AA.c_str(), BB);
        md5((uint8_t *)oao, strlen(oao), result);
        sprintf(oao, "%lld%s", BB, AA.c_str());
        md5((uint8_t *)oao, strlen(oao), result);
    }
    end = clock();
    printf("Loading done\n");
    printf("sprintf * 1000000 consumes %lf ms!\n", (double)(end-start));
    printf("sprintf * 1000000 consumes %lf s!\n", (double)(end-start)/CLOCKS_PER_SEC);
    return 0;
}
